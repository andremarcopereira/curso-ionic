import { Injectable } from '@angular/core';

const config_key:string = "config";

@Injectable()
export class ConfigProvider {

  private config = {
    showSlide: false,
    name: ""
  };

  constructor() {
  }

  getConfig():any {
    return localStorage.getItem(config_key);
  }

  setConfig(showSlide?: boolean, name?: string){
    let config = {
      showSlide: false,
      name: ""
    };

    if(showSlide){
      config.showSlide = showSlide;
    }

    if(name){
      config.name = name;
    }

    localStorage.setItem(config_key, JSON.stringify(config));
  }

}
