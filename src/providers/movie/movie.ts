import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {env} from '../../../env/env';
/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {

  constructor(public http: HttpClient) {
    
  }

  getLatestMovies(){
    return this.http.get('https://api.themoviedb.org/3/movie/latest?api_key='+ env.apiKey3);
  }

  getPopularMovies(){
    return this.http.get('https://api.themoviedb.org/3/movie/popular?api_key='+ env.apiKey3);
  }

}
